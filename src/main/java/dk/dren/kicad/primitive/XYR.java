package dk.dren.kicad.primitive;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.decimal4j.immutable.Decimal6f;

@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
public class XYR extends Point {
    private Decimal6f rotation;

    public XYR(Point p) {
        super(p);
        rotation = Decimal6f.ZERO;
    }

    public Point withoutRotation() {
        return new Point(getX(), getY());
    }

    public XYR(Decimal6f x, Decimal6f y, Decimal6f rotation) {
        super(x, y);
        this.rotation = rotation;
    }

    public XYR diff(XYR xyr) {
        return new XYR(
                xyr.getX().subtract(getX()),
                xyr.getY().subtract(getY()),
                xyr.getRotation().subtract(getRotation())
                );
    }

    @Override
    public String toString() {
        return "("+getX()+" "+getY()+" "+getRotation()+")";
    }

    public XYR add(XYR xyr) {
        return new XYR(getX().add(xyr.getX()),
                getY().add(xyr.getY()),
                getRotation().add(xyr.getRotation()));

    }
}
