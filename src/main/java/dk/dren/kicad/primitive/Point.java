package dk.dren.kicad.primitive;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.decimal4j.immutable.Decimal6f;

@NoArgsConstructor
@AllArgsConstructor
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Point implements Comparable<Point> {
    private Decimal6f x;
    private Decimal6f y;

    public Point(Point p) {
        x = p.x;
        y = p.y;
    }

    public Point(double x, double y) {
        this.x = Decimal6f.valueOf(x);
        this.y = Decimal6f.valueOf(y);
    }

    public Point(long x, long y) {
        this.x = Decimal6f.valueOf(x);
        this.y = Decimal6f.valueOf(y);
    }

    @Override
    public int compareTo(Point o) {
        int xc = x.compareTo(o.x);
        if (xc != 0) {
            return xc;
        }

        return y.compareTo(o.y);
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Point)) {
            return false;
        }
        return this.compareTo((Point)o) == 0;
    }
}
