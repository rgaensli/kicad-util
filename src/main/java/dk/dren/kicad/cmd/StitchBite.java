package dk.dren.kicad.cmd;

import dk.dren.kicad.pcb.Vector;
import dk.dren.kicad.primitive.Point;
import lombok.Value;

@Value
public class StitchBite {
    private final Point a;
    private final Point b;
    private final Vector direction;
}
