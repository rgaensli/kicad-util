package dk.dren.kicad.pcb;

import dk.dren.kicad.primitive.Point;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.decimal4j.immutable.Decimal6f;

@Getter
@RequiredArgsConstructor
public class Vector {
    private final Decimal6f x;
    private final Decimal6f y;

    public Vector(double x, double y) {
        this.x = Decimal6f.valueOf(x);
        this.y = Decimal6f.valueOf(y);
    }

    public static Vector between(Point a, Point b) {
        return new Vector(
                b.getX().subtract(a.getX()),
                b.getY().subtract(a.getY()));
    }

    public Decimal6f length() {
        return x.square().add(y.square()).sqrt();
    }

    public Vector unit() {
        return multiply(length().invert());
    }

    public Vector multiply(Decimal6f factor) {
        return new Vector(x.multiply(factor), y.multiply(factor));
    }

    public Vector normal() {
        return new Vector(y, x.negate());
    }

    public Line offset(Line line) {
        return new Line(offset(line.getStart()), offset(line.getEnd()));
    }

    public Point offset(Point point) {
        return new Point(point.getX().add(x), point.getY().add(y));
    }

    public double dot(Vector other) {
        return x.doubleValue()*other.getX().doubleValue() + y.doubleValue() * other.getY().doubleValue();
    }

    public Point toPoint() {
        return new Point(x,y);
    }

    public double angle(Vector ev) {
        Vector unit = unit();
        Vector otherUnit = ev.unit();
        double dot = unit.dot(otherUnit);
        double angleWithoutSign = Math.acos(dot);
        if (Double.isNaN(angleWithoutSign)) {
            if (dot > 1) {
                return 0;
            } else if (dot < 0) {
                angleWithoutSign = Math.PI/2;
            } else {
                throw new RuntimeException("Got bad angle between vectors: "+unit+" vs. "+otherUnit+": "+angleWithoutSign+" with dot product "+dot);
            }
        }
        double sign = Math.signum(unit.normal().dot(otherUnit));
        return sign * angleWithoutSign;
    }

    public double angle() {
        return Math.atan2(y.doubleValue(), x.doubleValue());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj instanceof Vector) {
            Vector o = (Vector) obj;
            return o.x.equals(x) && o.y.equals(y);
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "("+x+","+y+")";
    }

    public Vector multiply(double radius) {
        return multiply(Decimal6f.valueOf(radius));
    }

    /**
     * Rotates the vector
     *
     * @param angle The angle to rotate the vector
     * @return The rotated vector
     */
    public Vector rotate(double angle) {
        if (Double.isNaN(angle)) {
            throw new IllegalArgumentException("Need an angle, got: "+angle);
        }
        double cos = Math.cos(angle);
        double sin = Math.sin(angle);
        double xd = x.doubleValue();
        double yd = y.doubleValue();
        return new Vector(xd * cos - yd * sin,
                          xd * sin + yd * cos);
    }

    public Vector add(Vector other) {
        return new Vector(x.add(other.getX()), y.add(other.getY()));
    }
}
