package dk.dren.kicad.layoutclone;

import com.fasterxml.jackson.databind.ObjectMapper;
import dk.dren.kicad.pcb.Module;
import dk.dren.kicad.pcb.Position;
import dk.dren.kicad.pcb.RawNode;
import dk.dren.kicad.pcb.Line;
import dk.dren.kicad.primitive.Point;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

@NoArgsConstructor
@Data
public class CloneResult {

    private final Set<String> modulesMoved = new TreeSet<>();
    private final List<Point> viasCreated = new ArrayList<>();
    private final List<Line> segmentsCreated = new ArrayList<>();
    private File directory;
    private String pcbFile;
    private String backupPcbFile;
    private String reportFile;
    private int oldElementsRemoved;

    public void addMovedModule(Module module) {
        modulesMoved.add(module.getReference().toString());
    }

    public void addVia(RawNode via) {
        viasCreated.add(((Position)via.getProperty(Position.AT)).withoutRotation());
    }

    public void addSegment(RawNode segment) {
        segmentsCreated.add(Line.of(segment));
    }

    public void store(File reportFile) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(reportFile, this);
    }

    public static CloneResult load(File reportFile) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(reportFile, CloneResult.class);
    }

    @Override
    public String toString() {
        String res = "Performed the clone and modified "+getPcbFile()+"\n";
        if (getOldElementsRemoved() >= 0) {
            res += "* Old vias and segments from previous clone removed: " + getOldElementsRemoved()+"\n";
        }
        res += "* Modules moved: "+getModulesMoved().size()+"\n";
        res += "* Vias created: "+getViasCreated().size()+"\n";
        res += "* Segments created: "+getSegmentsCreated().size()+"\n";
        res += "* Backup stored in the backups subdir called: "+backupPcbFile+"\n";
        return res;
    }
}
